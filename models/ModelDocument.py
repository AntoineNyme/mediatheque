'''
    Author: Alexis Walle
    Représente une entrée dans la table document
'''

from models.FormError import FormError
from models.Model import Model


class ModelDocument(Model):
    def __init__(self, bdd, donnees={}):
        self.table = "Document"
        super().__init__(bdd)

        #   valeurs par défault
        self.support = "livre"
        self.disponible = "disponible"

        #   Pour hydrater le modèle avec des valeurs précises (lors d'un select) ou par défault
        self._Model__id = donnees.pop("id", 0) if donnees.get("id") else donnees.pop("idDocument", 0)
        self.titre = donnees.get("titre", "")
        self.support = donnees.pop("support", "livre")
        self.auteur = donnees.pop("auteur", "")
        self.datePublication = donnees.pop("datePublication", 0)
        self.editeur = donnees.pop("editeur", "")
        self.resume = donnees.pop("resume", "")
        self.note = donnees.pop("note", 0)
        self.disponible = donnees.pop("disponible", "disponible")

    @property
    def titre(self):
        return self.__titre

    @titre.setter
    def titre(self, titre):
        if 3 <= len(titre) <= 50:
            self.__titre = titre
            self.estModifie()

    @property
    def auteur(self):
        return self.__auteur

    @auteur.setter
    def auteur(self, auteur):
        self.__auteur = auteur
        self.estModifie()

    @property
    def datePublication(self):
        return self.__datePublication

    @datePublication.setter
    def datePublication(self, date_publication):
        try:
            date_publication = int(date_publication)
        except:
            raise FormError("datePublication", date_publication, "doit être un entier (timestamp)")

        self.__datePublication = date_publication
        self.estModifie()

    @property
    def resume(self):
        return self.__resume

    @resume.setter
    def resume(self, resume):
        self.__resume = resume
        self.estModifie()

    @property
    def editeur(self):
        return self.__editeur

    @editeur.setter
    def editeur(self, editeur):
        self.__editeur = editeur
        self.estModifie()

    @property
    def support(self):
        return self.__support

    @support.setter
    def support(self, support):
        if support in ["livre", "dvd", "cd", "revue", "cdrom", "livre numérique"]:
            self.__support = support
            self.estModifie()
        else:
            raise FormError("Support", support,
                            'les valeurs autorisées sont ["livre", "dvd", "cd", "revue", "cdrom", "livre numérique"]')

    @property
    def note(self):
        return self.__note

    @note.setter
    def note(self, note):
        if not 0 <= int(note) <= 5:
            raise FormError("note", note, "doit être compris entre 0 et 5")
        self.__note = note
        self.estModifie()

    @property
    def disponible(self):
        return self.__disponible

    @disponible.setter
    def disponible(self, estDisponible):
        if estDisponible in ["disponible", "emprunte", "perdu", "jete"]:
            self.__disponible = estDisponible
            self.estModifie()
        else:
            raise FormError("status", estDisponible,
                            'doit être une chaine de caractère comme "disponible", "emprunte", "perdu", "jete"')

    def createTable(self):
        self.bdd.execute("DROP TABLE IF EXISTS Document")
        return super().createTable(
            "CREATE TABLE Document(id INTEGER PRIMARY KEY ASC, titre TEXT, support TEXT, auteur TEXT, datePublication INTEGER, editeur TEXT, resume TEXT, note NUM, disponible TEXT DEFAULT 'disponible')")

    def update(self):
        donnees = [self.titre, self.support, self.auteur, self.datePublication, self.editeur, self.resume,
                   self.note, self.disponible, self.id]
        return super().update(
            "UPDATE Document SET titre = ?, support = ?, auteur = ?, datePublication = ?, editeur = ?, resume = ?, note = ?, disponible = ? WHERE id = ?",
            donnees)

    def delete(self):
        donnees = [self.id]
        return super().delete("DELETE FROM Document WHERE id = ?", donnees)

    def insert(self):
        donnees = [self.titre, self.support, self.auteur, self.datePublication, self.editeur, self.resume, self.note,
                   self.disponible]
        return super().insert(
            "INSERT INTO Document(titre, support, auteur, datePublication, editeur, resume, note, disponible) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            donnees)
