'''
    Author: Clément Mauabnt
    Contrôle les requêtes de connexion et d'inscription
'''

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from models.baseDonnees import get_db
from models.ModelUtilisateur import ModelUtilisateur
from models.UtilisateurRepository import UtilisateurRepository

bp = Blueprint('auth', __name__, url_prefix='/connexion')


@bp.route('/inscription', methods=('GET', 'POST'))
def inscription():
    db = get_db()
    utilisateurRepository = UtilisateurRepository(db)

    if request.method == 'POST':
        email = request.form['email']
        password = request.form['MotdePasse']
        print(request.form.get("MotdePasse"),request.form.get("MotdePasseBis"))

        if not email:
            flash('Une adresse mail est requise.',"error")
        elif not password:
            flash('Un mot de passe est requis.', "error")
        elif request.form.get('MotdePasse') != request.form.get('MotdePasseBis'):
            flash('Les deux mots de passe ne sont pas identiques.', "error")
        elif utilisateurRepository.get(email):
            flash('L\'utilisateur {} est déjà inscrit.'.format(email), "error")
        else:
            utilisateur = ModelUtilisateur(db)
            utilisateur.email = email
            utilisateur.nom = request.form.get("nom")
            utilisateur.prenom = request.form.get("prenom")
            utilisateur.profession = request.form.get("profession")
            utilisateur.motdepasse = generate_password_hash(password)
            utilisateur.insert()
            flash("Inscription réussie !","info")
            return redirect(url_for('auth.login'))

    return render_template('auth/inscription.html')


@bp.route('/', methods=['GET', 'POST'])
def login():
    #   On déconnecte l'utilisateur si il est déjà connecté
    if session.get('email'):
        session.clear()
        return redirect(url_for('urls.accueil'))

    #   Sinon on fait les vérifications de connexion
    if request.method == 'POST':

        email = request.form['email']
        password = request.form['password']
        db = get_db()
        utilisaterRepository = UtilisateurRepository(db)

        connexion = utilisaterRepository.credimentalsCorrects(email, password)

        if connexion:
            session.clear()
            session['email'] = email
            session['prenom'] = connexion[1]
            session['nom'] = connexion[2]
            if connexion[3] == 1:
                session['admin'] = True
                flash("Connexion administrateur réussie", 'info')
            else:
                flash("Connexion utilisateur réussie", 'info')

            return redirect(request.args.get("next") or url_for("urls.empruntsUser"))

        flash("Erreur d'identification", 'error')

    return render_template('auth/connexion.html')
