'''
    Author: Clément Maubant
    Fichier testiné à insérer des données de test dans la base de donnée, des exemples de livres
'''

def seed(db):
    donnees = [
        ('Le Cid', 'livre', 'Corneille', "1637", 'Rageot', 'Un bon livre', 3, 'disponible'),
        ('Let It Be', 'cd', 'The Beatles', "1970", 'Apple Corps', 'Let It Be est le dernier single anglais du groupe. Paul McCartney a déclaré que la chanson lui avait été inspirée par un rêve', 5, 'disponible'),
        ("Les Aventures d'Alice au pays des merveilles", "livre", "Lewis Carroll","1865", "Macmillan and Co", "Alice s'ennuie auprès de sa sœur qui lit un livre (« sans images, ni dialogues ») tandis qu'elle ne fait rien. « À quoi bon un livre sans images, ni dialogues ? », se demande Alice. Mais voilà qu'un lapin blanc aux yeux roses vêtu d'une redingote avec une montre à gousset à y ranger passe près d'elle en courant.", 4, "disponible")
    ]
    db.executemany(
        "INSERT INTO document(titre, support, auteur, datePublication, editeur, resume, note, disponible) VALUES (?,?,?,?,?,?,?,?)", donnees)
