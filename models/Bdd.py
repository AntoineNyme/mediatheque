'''
    Author: Clément Maubant
    Classe d'abstraction de la base de donnée, pour rendre facile un éventuel changement
    de système de gestion de base de donnée
'''
import sqlite3
import os


class Bdd:
    def __init__(self, nomBase):
        self.nomBase = nomBase

    def connexion(self):
        basedir = os.path.abspath(self.nomBase)
        print("Connexion db ",basedir)
        self._connection = sqlite3.connect(basedir)
        if self._connection:
            return True
        return False

    def execute(self, requette, donnees=[]):
        reponse = self._connection.execute(requette, donnees)
        self._connection.commit()
        return reponse

    def executemany(self, requette, donnees):
        reponse = self._connection.executemany(requette, donnees)
        self._connection.commit()
        return reponse


    def __del__(self):
        self._connection.close()

    #   Pour extraire les clées du sqlite3.Cursor et mettre le résultat sous forme de dictionaire
    def rowToDico(self, row, keys):
        keys = [v[0] for v in keys]
        return dict(zip(keys, row))
