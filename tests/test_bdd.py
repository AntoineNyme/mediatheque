'''
    Author: Clément Maubant
    Permet de tester la connexion à la base de données
'''

import unittest
import sqlite3
from models.Bdd import Bdd


class TestBdd(unittest.TestCase):
    def setUp(self):
        self._bdd = Bdd("test.db")
        self._bdd.connexion()

    def testConnexion(self):
        reponse = self._bdd.execute("SELECT 1")
        self.assertIsInstance(reponse, sqlite3.Cursor)
        self.assertEqual(reponse.fetchone(), (1,))

    def testTest(self):
        self.assertTrue(True, "tests ok")
