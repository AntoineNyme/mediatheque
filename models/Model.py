'''
    Author: Clément Maubant
    Classe abstraite représentant une entrée de base de donnée générique
'''

from abc import abstractmethod, ABCMeta


class Model:
    @abstractmethod
    def __init__(self, bdd):
        self._Model__bdd = bdd
        self._modifie = True
        self._rempli = False
        self._Model__id = 0

    @abstractmethod
    def createTable(self, requette):
        return self.bdd.execute(requette).fetchone() == None

    @abstractmethod
    def insert(self, requette, donnees) -> int:
        reponse = self.bdd.execute(requette, donnees)
        if reponse.lastrowid:
            self._Model__id = reponse.lastrowid
            self._modifie = False
            return True
        return False

    @abstractmethod
    def update(self, requette, data) -> int:
        if self.id == 0:
            raise Exception("Update nécessite que le model ait été inséré avec un id")
        if self._modifie:
            reponse = self.bdd.execute(requette, data)
            if reponse.rowcount == 1:
                self._modifie = False
                return True
        return False

    @abstractmethod
    def delete(self, requette, data) -> int:
        if self.id == 0:
            raise Exception("Delete nécessite que le model ait été inséré avec un id")
        reponse = self.bdd.execute(requette, data)
        if reponse.rowcount == 1:
            self._Model__id = None
            self._modifie = False
            return True
        return False

    #   Méthode non utilisée
    def hydratation(self, donnees):
        for nom, val in donnees:
            if callable(self[nom]):
                self[nom] = val
            self.estModifie()

    @property
    def id(self):
        if self.__id == 0:
            return 0
        return self.__id

    @property
    def bdd(self):
        return self.__bdd

    def estModifie(self):
        self._modifie = True

    def estRempli(self):
        self._rempli = True

    #   Méthodes de maj
    def save(self):
        if self.__id:
            self.update()
        else:
            self.insert()
