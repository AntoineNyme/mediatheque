'''
    Author: Clément Maubant
    Permet de rendre accessible la base de donnée dans toute l'application
'''

from flask import current_app, g
from models.Bdd import Bdd

#   Méthode utilisée dans toute l'application pour obtenir une instance de Bdd, fin de faire des requêtes
def get_db():
    if 'db' not in g:
        print(current_app.config['DATABASE'])
        g.db = Bdd(current_app.config['DATABASE'])
        g.db.connexion()

    return g.db


def close_db(exception):
    db = g.pop('db', None)
    if db is not None:
        del db

#   Permet de lier Flask à la méthode close_db afin de fermer automatiquement la connexion
#   et d'enregistrer les modifications
def init_app(app):
    app.teardown_appcontext(close_db)
