'''
    Author: Clément Maubant
    Exception utilisée lorsque que le champ d'un formulaire n'a pas une valeur autorisées
'''

class FormError(ValueError):

    def __init__(self, champ, valeur, message):
        self.champ = champ
        self.message = message
        self.valeur = valeur
        super().__init__(message)

    def __str__(self):
        return f'Erreur sur le champ {self.champ} : {self.message}, "{self.valeur}" transmis'