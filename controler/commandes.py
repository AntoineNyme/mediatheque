'''
    Author: Clément Maubant
    Fonctions exécutables par le webmaster, à des fins de maintenance
'''

from flask import Blueprint, current_app
from models.baseDonnees import get_db
import click
import os

from models.ModelUtilisateur import ModelUtilisateur
from models.ModelDocument import ModelDocument
from models.ModelEmprunt import ModelEmprunt
from models.documentsSeeder import seed as d_seeder

from werkzeug.security import generate_password_hash

commandes = Blueprint('commandes', __name__)

#   Pour réinitialiser la base de donnée
@commandes.cli.command('init-db')
def init_db():
    db = get_db()

    utilisateur = ModelUtilisateur(db)
    utilisateur.createTable()

    document = ModelDocument(db)
    document.createTable()

    emprunt = ModelEmprunt(db)
    emprunt.createTable()
    del db
    click.echo('Base de données créée.\n')
    click.echo("Fichier: " + current_app.config['DATABASE'])

#   Pour créer un utilisateur administrateur
@commandes.cli.command("create-admin")
@click.argument("email")
@click.argument("mdp")
def create_admin(email, mdp):
    db = get_db()
    utilisateur = ModelUtilisateur(db)
    utilisateur.email = email
    utilisateur.motdepasse = generate_password_hash(mdp)
    utilisateur.prenom = ""
    utilisateur.nom = ""
    utilisateur.profession = ""
    utilisateur.peutEmprunter = True
    utilisateur.estAdmin = True
    if utilisateur.insert():
        click.echo('Utilisateur créé !')
    else:
        click.echo('Echec de la création')

#   Pour remplir la base de donnée avec des livres à des fins de tests
@commandes.cli.command('seed')
def seeder():
    db = get_db()
    d_seeder(db)

    click.echo('Seeder exécuté.')

#   Commande de débeugage utilisée lors de l'intégration sur Heroku
@commandes.cli.command('test-db')
def test_db():
    db2 = get_db()
    click.echo("Fichier: " + current_app.config['DATABASE'] + "\n\nTables:")
    tables = db2.execute("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';")
    for r in tables:
        click.echo(r[0])

    chemin_absolu = os.path.abspath(current_app.config['DATABASE'])
    click.echo("\n\nChemin absolu: "+chemin_absolu)
