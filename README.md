# README #

Projet informatique
Consiste à créer un système de gestion de fonds documentaires léger, accessible depuis un navigateur web et fonctionnel.
Il permet d'assurer le suivi et l'inventaire de documents (livres, dvd, cd...), les prêts de documents, la personalisation entre autre.
L'utilisation est possible dans un cadre privé (collection personnelle) ou à plus grande échelle, avec plusieurs emprunteurs.


### Installation ###

Le projet nécessite Python 3 et Flask pour fonctionner. Une fois le projet récupéré (manuellement ou avec un git clone), 
Il est conseillé d'installé un virtualenv. 
Il faut ensuite installer les dépendance du projet avec la commande :

* pip install -r requirements.txt

Plus de détail est disponible à l'adresse https://flask.palletsprojects.com/en/1.1.x/installation/.


### Configuration ###

Normalement le projet est conçu pour fonctionner directement à cette étape. Néanmoins, il est fortement conseillé de créer un ".env":

* renommer le fichier ".env.exemple" en ".env"
    * écrire une chaine de caractères alléatoire dans "SECRET_KEY"
    * paramétrer les différentes clées du fichier ".env"

Si vous avez installé le projet dans un virtualenv, vous pouvez effectuer les étapes suivantes :
* lancez l'initialisation de la base de données en tapant "flask commandes init-db", sans cela ce sont les données d'exemple qui sont utilisées
* créez un admin avec la commande "flask commandes create-admin"


### Lancer le serveur ###

Cas générique
* py -3 app.py

Avec un virtualenv, la commande suivante est disponible
* flask run