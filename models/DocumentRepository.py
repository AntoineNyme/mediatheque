'''
    Author: Alexis Walle
    Permet de faire des selects sur la table document et de renvoyer des ModelDocument
'''

from models.ModelDocument import ModelDocument
from models.ModelEmprunt import ModelEmprunt
from models.Repository import Repository


class DocumentRepository(Repository):

    def __init__(self, bdd):
        super().__init__(bdd)

    def findRecents(self, limite=5):
        """
        :param limite: determine la limite des derniers livres
        :return: Permet de renvoyer les n derniers livres de la base de données
        """
        documents = []
        resultCursor = self._bdd.execute("SELECT * FROM document ORDER BY id DESC LIMIT ?", [limite])
        for document in resultCursor:
            dico = self._bdd.rowToDico(document, resultCursor.description)
            documents.append(ModelDocument(self._bdd, dico))
        return documents

    def findQuerry(self, querry, order, page=0, limite=10):
        """
        :param querry: requête
        :param order: determine l'ordre de la liste (chronologique, support, note, ...)
        :param page: numéro de la page
        :param limite: limite de documents
        :return: Permet de renvoyer les documents correspondant à une requête
        """
        documents = []
        order = " ORDER BY "+order if order != "" else ""
        querry = "%" + querry + "%"
        resultCursor = self._bdd.execute(
            "SELECT * FROM document WHERE disponible IN ('disponible', 'emprunte') AND (titre LIKE ? OR auteur LIKE ? OR resume LIKE ?)"+order+" LIMIT ? OFFSET ?",
            [querry, querry, querry, limite, (page - 1) * limite])
        for document in resultCursor:
            dico = self._bdd.rowToDico(document, resultCursor.description)
            documents.append(ModelDocument(self._bdd, dico))
        return documents


    def finddocs(self, order, page=0, limite=10):
        """
        :param order: determine l'ordre de la liste (chronologique, support, note, ...)
        :param page: numéro de la page
        :param limite: limite de documents
        :return: Permet de renvoyer les documents s'il n'y a pas de critère de recherche
        """
        documents = []
        order = " ORDER BY "+order if order != "" else ""
        resultCursor = self._bdd.execute(
            "SELECT * FROM document WHERE disponible IN ('disponible', 'emprunte')"+order+" LIMIT ? OFFSET ?",
            [limite, (page - 1) * limite])
        for document in resultCursor:
            dico = self._bdd.rowToDico(document, resultCursor.description)
            documents.append(ModelDocument(self._bdd, dico))
        return documents

    def totalDocuments(self):
        """
        :return: Nombre total de documents, tout confondu
        """
        resultCursor = self._bdd.execute("SELECT COUNT(*) AS total FROM document")
        reponse = resultCursor.fetchone()
        if reponse:
            return reponse[0]
        return 0


    def totalDocumentsQuerry(self, querry):
        """
        :param querry: requête
        :return: Nombre total de documents correspondant à une recherche
        """
        querry = "%" + querry + "%"
        reponse = self._bdd.execute(
            "SELECT COUNT(*) FROM document WHERE disponible IN ('disponible', 'emprunte') AND (titre LIKE ? OR auteur LIKE ? OR resume LIKE ?)",
            [querry, querry, querry])
        reponse = reponse.fetchone()
        if reponse:
            return reponse[0]
        return 0


    def finddetail(self, id):
        """
        :param id: identifiant du document
        :return: retourne la sélection d'un document en particulier
        """

        # on selectionne un livre
        resultCursor = self._bdd.execute("SELECT * FROM document WHERE id=? LIMIT 1", [id])
        document = resultCursor.fetchone()
        if document:
            dico = self._bdd.rowToDico(document, resultCursor.description)
            return ModelDocument(self._bdd, dico)
        return

    def listEmprunts(self, id_document):
        """
        :param id_document: identifiant du document
        :return: Pour afficher la liste des emprunts d'un document particulier
        """
        emprunts = []
        resultCursor = self._bdd.execute(
            "SELECT e.id, e.idUtilisateur, e.idDocument, u.nom, u.prenom, u.profession, u.email FROM Emprunt e INNER JOIN Utilisateur u ON u.id = e.idUtilisateur WHERE e.idDocument=?",
            [id_document])

        for emprunt in resultCursor:
            dico = self._bdd.rowToDico(emprunt, resultCursor.description)
            emprunts.append(ModelEmprunt(self._bdd, dico))
        return emprunts

    def nombreEmprunt(self, id_document):
        """
        :param id_document: identification du document
        :return: retourne le nombre de fois qu'un document a été emprunté
        """
        resultCursor = self._bdd.execute("SELECT COUNT(*) FROM Emprunt WHERE idDocument = ?", [id_document])
        reponse = resultCursor.fetchone()
        if reponse:
            return reponse[0]
        return 0


    def nombreEmpruntTotal(self):
        """
        :return: retourne le nombre total d'emprunts (utilisé pour des statistiques)
        """
        resultCursor = self._bdd.execute("SELECT COUNT(*) FROM Emprunt", [])
        reponse = resultCursor.fetchone()
        if reponse:
            return reponse[0]
        return 0

    def statsDocuments(self):
        """
        :return:  retourne les statistiques sur le nombre de document par support
        """
        resultCursor = self._bdd.execute("SELECT support, COUNT(*) FROM document GROUP BY support")
        if resultCursor:
            return resultCursor
        return []
