'''
    Author: Clément Maubant
    Point d'entrée dans l'application
'''

#   Le serveur s'appuie sur le framwork Flask pour gérer les connexions HTTP
from flask import Flask, render_template
from os import environ
from controler.commandes import commandes

#   Modules constituant les différentes parties du programme
from controler.routing import urlsr
from controler.bp_admin import bp_admin
from controler.auth import bp

import models.baseDonnees as baseDonnees

app = Flask(__name__)

#   Permet d'enregistrer des données de configuration (par exemple le fichier de base de donnée)
#   afin de d'y accéder dans tout le programme
app.config.from_pyfile('.env', silent=True)
if not app.config.get("DATABASE"):
    app.config["DATABASE"] = environ.get("DATABASE", "models/database.db")
    app.config["SECRET_KEY"] = environ.get("SECRET_KEY")
    app.config["PAR_PAGE"] = environ.get("PAR_PAGE", 2)
    app.config["NOM_APPLICATION"] = environ.get("NOM_APPLICATION", "Projet Médiathèque")

#   Cette fonction fait l'association entre le module gérant la base de données
#   et le reste de l'application
baseDonnees.init_app(app)

#   Chaque partie du programme est gérée dans un module séparé, qui sont :
#   *   les pages accessibles à tout le monde
#   *   les pages réservées aux administrateurs
#   *   les pages de connexions et d'inscription
app.register_blueprint(urlsr)       #   Pages publiques
app.register_blueprint(bp_admin)    #   Pages administrateur
app.register_blueprint(bp)          #   Pages de connexion

#   Déclaration des "commandes", permettant d'exécuter des morceaux de programme spécifique
#   dans une console, typiquement l'invité de commande système.
#   Cela permet par exemple de créer manuellement un compte administrateur.
app.register_blueprint(commandes)


#   On défini ici les pages d'erreurs, s'affichant si l'utilisateur veut accéder à une page
#   qui n'existe pas ou dont l'accès est réservé aux administrateurs.
@app.errorhandler(404)  #   Erreur 4040: Page non trouvée
def erreur404(e):
    return render_template('erreur404.html'), 404


@app.errorhandler(403)  #   Erreur 403: Accès interdit
def erreur403(e):
    return render_template('erreur403.html'), 403


#   Enfin, on démarre ici le serveur pour qu'il écoute les requêtes entrantes sur le port 5000.
if __name__ == '__main__':
    app.run(threaded=True, port=5000)
