'''
    Author: Alexis Walle, Clément Maubant
    Permet de contrôler les requêtes accessible à tous
'''

import json

from flask import Blueprint, render_template, current_app, request, session, url_for, redirect, Response
from models.Bdd import Bdd
from models.baseDonnees import get_db

from models.DocumentRepository import DocumentRepository
from flask_paginate import Pagination, get_page_parameter

from models.UtilisateurRepository import UtilisateurRepository

urlsr = Blueprint('urls', __name__, )


#   Page d'accueil
@urlsr.route('/')
def accueil():
    configuration = json.load(open("configuration.json"))
    docRepository = DocumentRepository(get_db())
    documentsRecents = docRepository.findRecents()

    return render_template("index.html", documents=documentsRecents, configuration=configuration)


#   Page affichant la liste des livres
@urlsr.route('/livres')
def livres():
    listeOrdre = {"id": "Chronologique", "titre": "Titre", "auteur": "Auteur"}
    docRepository = DocumentRepository(get_db())
    page = int(request.args.get("page", default=1))

    order = request.args.get("order", "id") if listeOrdre.get(request.args.get("order", "id")) else ""

    #   Si il y a une recherche
    if request.args.get("q"):
        documents = docRepository.findQuerry(request.args.get("q"), order, page, current_app.config['PAR_PAGE'])
        total = docRepository.totalDocumentsQuerry(request.args.get("q"))
    else:
        documents = docRepository.finddocs(order, page, current_app.config['PAR_PAGE'])
        total = docRepository.totalDocuments()

    #   Pour gérer la pagination
    pagination = Pagination(page=page, total=total, per_page=current_app.config['PAR_PAGE'], css_framework="bootstrap4",
                            bs_version=4)
    return render_template('livres.html', documents=documents, pagination=pagination,
                           querry=request.args.get("q", False), listeOrdre=listeOrdre)


#   Page de détail d'un livre
@urlsr.route('/livre/<int:id>')
def detail(id):
    docRepository = DocumentRepository(get_db())
    document = docRepository.finddetail(id)
    nbEmprunts = docRepository.nombreEmprunt(id)
    return render_template('detail.html', document=document, nbEmprunts=nbEmprunts)


#   Page affichant la liste des emprunts d'un utilisateur connecté
@urlsr.route('/emprunts')
def empruntsUser():
    #   On redirige les utilisateurs qui ne sont pas connectés
    if "email" not in session:
        return redirect(url_for("auth.login", next=request.url))

    userRepository = UtilisateurRepository(get_db())
    emprunts = userRepository.emprunts(session['email'])
    utilisateur = userRepository.get(session['email'])

    return render_template('emprunts.html', emprunts=emprunts, utilisateur=utilisateur)


#   Fichier utilisé pour personalisé dynamiquement les couleurs de l'application
@urlsr.route("/color.css")
def color():
    configuration = json.load(open("configuration.json"))
    texte = "body{ background-color: " + configuration.get("bg_color") + "; color:" + configuration.get(
        "text_color") + ";  } "
    texte += "main{background-color: " + configuration.get("front_color") + ";} "
    texte += "nav.navigation{background-color: " + configuration.get("nav_color") + ";} "
    return Response(texte, mimetype='text/css')


#   Routes de démonstration, tout à fait inutiles ici
@urlsr.route('/secret')
def index():
    return 'ceci est autre chose de secret'


@urlsr.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)
