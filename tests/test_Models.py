'''
    Author: Clément Maubant
    Permet tester le bon comportement des classes de Modele
'''

from models.ModelDocument import ModelDocument
from models.ModelUtilisateur import ModelUtilisateur
from models.ModelEmprunt import ModelEmprunt
from models.Bdd import Bdd

import unittest


class TestModel(unittest.TestCase):
    def setUp(self):
        self._bdd = Bdd("test.db")
        self._bdd.connexion()

    def testCreation(self):
        #   Supression puis création des tables
        utilisateur = ModelUtilisateur(self._bdd)
        self.assertTrue(utilisateur.createTable())

        document = ModelDocument(self._bdd)
        self.assertTrue(document.createTable())

        emprunt = ModelEmprunt(self._bdd)
        self.assertTrue(emprunt.createTable())

        #   Vérification de la bonne création avec une insertion, qui doit à chaque fois insérer une ligne
        rep = self._bdd.execute(
            "INSERT INTO Utilisateur(nom, prenom, profession, email, peutEmprunter, motdepasse, estAdmin) VALUES ('Simson', 'Lisa', 'Etudiante', 'lisa.simson@wanadoo.com', 1, 'password', 0)")
        self.assertEqual(rep.rowcount, 1)
        rep = self._bdd.execute(
            "INSERT INTO document(titre, support, auteur, datePublication, editeur, resume, note, disponible) VALUES ('Le Cid', 'livre', 'Corneille', 10115, 'Rageot', 'Un bon livre', 3, 'disponible')")
        self.assertEqual(rep.rowcount, 1)
        rep = self._bdd.execute(
            "INSERT INTO emprunt(idDocument, idUtilisateur, dateLimite, dateRetour) VALUES (1,1,0,0)")
        self.assertEqual(rep.rowcount, 1)

    def testModelUtilisateur(self):
        #   L'utilisateur est créé à partir du modèle, puis modifié et supprimé
        utilisateur = ModelUtilisateur(self._bdd)
        utilisateur.nom = "Simson"
        utilisateur.prenom = "Homer"
        utilisateur.profession = "Garagiste"
        utilisateur.email = "homer.simson@security-Springfield.com"
        utilisateur.motdepasse = '404notfound'
        utilisateur.peutEmprunter = False
        utilisateur.estAdmin = False

        self.assertTrue(
            utilisateur.insert()
        )
        self.assertIsNotNone(utilisateur.id)
        print("Id inséré:", utilisateur.id) #   L'id est automatiquement disponible après l'enregistrement

        utilisateur.peutEmprunter = True
        self.assertTrue(
            utilisateur.update()
        )

        self.assertTrue(
            utilisateur.delete()
        )

    def testModelDocument(self):
        #   le document est créé à partir du modèle, puis modifié et supprimé
        document = ModelDocument(self._bdd)
        document.titre = "Harry Potter et la chambre à coucher"
        document.auteur = "Alain Finkielkraut"
        document.datePublication = 10
        document.resume = "L'histoire d'une chambre et d'un serpent plein de secrets"
        document.editeur = "Flamarion"
        document.support = "livre"
        document.note = 5
        document.disponible = "disponible"

        self.assertTrue(
            document.insert()
        )
        self.assertIsNotNone(document.id)
        print("Id inséré:", document.id)

        document.note = 4
        self.assertTrue(
            document.update()
        )

        self.assertTrue(
            document.delete()
        )

    def testModelEmprunt(self):
        #   l'emprunt est créé à partir du modèle, puis modifié et supprimé
        emprunt = ModelEmprunt(self._bdd)
        emprunt.idDocument = 1
        emprunt.idUtilisateur = 1
        emprunt.dateLimite = 11

        self.assertTrue(
            emprunt.insert()
        )
        self.assertIsInstance(emprunt.id, int)
        print("Id inséré:", emprunt.id)

        emprunt.dateRetour = 12
        self.assertTrue(
            emprunt.update()
        )

        self.assertTrue(
            emprunt.delete()
        )

        emprunt = ModelEmprunt(self._bdd, {"idUtilisateur": 1})
        self.assertEqual(1, emprunt.idUtilisateur)
