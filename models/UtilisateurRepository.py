'''
    Author: Alexis Walle
    Permet d'effectuer des select sur des utilisateurs
'''
from models.ModelEmprunt import ModelEmprunt
from models.ModelUtilisateur import ModelUtilisateur
from models.Repository import Repository

from werkzeug.security import check_password_hash, generate_password_hash


class UtilisateurRepository(Repository):

    def __init__(self, bdd):
        super().__init__(bdd)

    #   Vérification des login et mot de passe
    def credimentalsCorrects(self, login, mdp):
        resultCursor = self._bdd.execute(
            "SELECT motdepasse, prenom, nom, estadmin FROM Utilisateur WHERE email = ?", [login])
        reponse = resultCursor.fetchone()

        if reponse and len(reponse) > 1:
            if check_password_hash(reponse[0], mdp):
                return reponse
        return False

    #   Liste des emprunts d'un tulisateur donné
    def emprunts(self, email):
        emprunts = []
        resultCursor = self._bdd.execute(
            "SELECT e.id, e.idUtilisateur, e.idDocument, e.dateLimite, d.id, d.titre, d.auteur, d.support FROM Emprunt e INNER JOIN Document d ON d.id = e.idDocument INNER JOIN Utilisateur u ON u.id = e.idUtilisateur WHERE u.email = ?",
            [email])

        for emprunt in resultCursor:
            dico = self._bdd.rowToDico(emprunt, resultCursor.description)
            emprunts.append(ModelEmprunt(self._bdd, dico))
        return emprunts

    #   Permet d'obtenir un utilisateur particulier
    def get(self, email):
        resultCursor = self._bdd.execute(
            "SELECT * FROM Utilisateur WHERE email = ?",
            [email])
        emprunt = resultCursor.fetchone()

        if emprunt:
            dico = self._bdd.rowToDico(emprunt, resultCursor.description)
            return ModelUtilisateur(self._bdd, dico)
        return

    #   Permet d'obtenir le nombre d'utilisateur total
    def nombre(self):
        resultCursor = self._bdd.execute(
            "SELECT COUNT(*) FROM Utilisateur",
            [])
        result = resultCursor.fetchone()

        if result:
            return result[0]
        return 0

    #   Permet d'obtenir les derniers emprunts
    def derniersEmprunts(self):
        emprunts = []
        resultCursor = self._bdd.execute(
            "SELECT e.id, e.idUtilisateur, e.idDocument, e.dateLimite, d.id, d.titre, d.auteur, d.support, u.email, u.prenom, u.nom FROM Emprunt e LEFT JOIN Document d ON d.id = e.idDocument LEFT JOIN Utilisateur u ON u.id = e.idUtilisateur ORDER BY e.id DESC LIMIT 5",
            [])

        for emprunt in resultCursor:
            dico = self._bdd.rowToDico(emprunt, resultCursor.description)
            emprunts.append(ModelEmprunt(self._bdd, dico))
        return emprunts

    #   Permet d'obtenir les derniers utilisateurs
    def dernierUtilisateurs(self):
        utilisateurs = []
        resultCursor = self._bdd.execute(
            "SELECT * FROM Utilisateur ORDER BY id DESC LIMIT 5")

        for utilisateur in resultCursor:
            dico = self._bdd.rowToDico(utilisateur, resultCursor.description)
            utilisateurs.append(ModelUtilisateur(self._bdd, dico))
        return utilisateurs

    #   Permet d'ajouter un emprunt
    def ajoutEmprunt(self, idUtilisateur, idDocument, dateLimite, dateRetour):
        resultCursor = self._bdd.execute(
            "INSERT INTO Emprunt(idUtilisateur, idDocument, dateLimite, dateRetour) VALUES (?, ?, ?, ?)",
            [idUtilisateur, idDocument, dateLimite, dateRetour])

        return resultCursor.rowcount == 1
