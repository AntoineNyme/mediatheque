'''
    Author: Clément Maubant
    Permet de tester les repository
'''

from models.ModelDocument import ModelDocument
from models.Bdd import Bdd
from models.DocumentRepository import DocumentRepository
from models.ModelEmprunt import ModelEmprunt
from models.UtilisateurRepository import UtilisateurRepository

import unittest

class TestRepository(unittest.TestCase):
    def setUp(self):
        self._bdd = Bdd("test.db")
        self._bdd.connexion()

    def testDocumentRepository(self):
        documentRepository = DocumentRepository(self._bdd)

        recents = documentRepository.findRecents()
        self.assertIsNotNone(recents)
        self.assertIsInstance(recents[0], ModelDocument)
        self.assertIsInstance(recents[0].titre, str)

        #   La base de test contient Le Cid de Corneille
        self.assertGreaterEqual(len(documentRepository.findQuerry("Corneille", "id")), 1)
        self.assertIs(len(documentRepository.findQuerry("Dante", "id")), 0)

    def testUserRepository(self):
        utilisaterRepository = UtilisateurRepository(self._bdd)

        #   Vérification que le repository ne lève pas d'erreur et retourne faux en cas de mauvais login
        self.assertIs(utilisaterRepository.credimentalsCorrects("loggin bidon", "mdp incorrect"), False)

    def testEmpruntRepository(self):
        documentRepository = DocumentRepository(self._bdd)

        emprunts = documentRepository.listEmprunts(1)
        self.assertIsNotNone(emprunts)
        self.assertIsInstance(emprunts[0], ModelEmprunt)
        self.assertIsInstance(emprunts[0].idDocument, int)
