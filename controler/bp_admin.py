'''
    Author: Clément Maubant
    Contrôle des requêtes administrateur
'''

import functools
import json

from flask import Blueprint, render_template, redirect, url_for, request, session, abort, flash
from models.baseDonnees import get_db

from models.DocumentRepository import DocumentRepository
from models.FormError import FormError
from models.ModelDocument import ModelDocument
from models.UtilisateurRepository import UtilisateurRepository

bp_admin = Blueprint('urlsa', __name__, )


#   Fonction de protection des routes, pour empécher les non admin d'accéder à la page
def admin_required(func):
    @functools.wraps(func)
    def secure_function(*args, **kwargs):
        if "email" not in session:
            return redirect(url_for("auth.login", next=request.url))
        if not session.get("admin"):
            abort(403)
        return func(*args, **kwargs)

    return secure_function


#   Permet de vérifier les donnees des formulaires de modification des documents
def validation(document):
    if request.method == 'POST':
        try:
            document.titre = request.form['titre']
            document.auteur = request.form['auteur']
            # document.support = request.form['support']
            document.resume = request.form['resume']
            document.datePublication = request.form['datePublication']
            document.editeur = request.form['editeur']
            document.note = request.form['note']
            # document.disponible = request.form['disponible']

            if document.id and document.update():
                flash("Modifications enregistrées", "info")
                return True
            elif not document.id and document.insert():
                flash("Document ajouté", "info")
                return True
            else:
                flash("Erreur d'enregistrement", "error")
        except FormError as e:
            flash(str(e), "error")
    return False


@bp_admin.route('/livre/nouveau', methods=['GET', 'POST'])
@admin_required
def nouveau():
    bdd = get_db()
    document = ModelDocument(bdd)
    if validation(document):
        return redirect(url_for('urls.detail', id=document.id))
    return render_template('edition_livre.html', document=document)


@bp_admin.route('/livre/<int:id>/edition', methods=['GET', 'POST'])
@admin_required
def edition(id):
    bdd = get_db()
    docRepository = DocumentRepository(bdd)
    document = docRepository.finddetail(id)
    validation(document)

    return render_template('edition_livre.html', document=document)


@bp_admin.route('/livre/<int:id>/rendre')
@admin_required
def rendreLivre(id):
    bdd = get_db()
    docRepository = DocumentRepository(bdd)
    document = docRepository.finddetail(id)
    if document:
        if document.disponible == "emprunte":
            document.disponible = "disponible"
            document.update()
            flash("Document rendu disponible", "info")
        else:
            flash("Document non emprunté", "error")
    else:
        flash("Document non trouvé", "error")

    return redirect(url_for('urls.detail', id=document.id))


@bp_admin.route('/livre/<int:id>/supprimer', methods=['POST'])
@admin_required
def supprimer(id):
    bdd = get_db()
    docRepository = DocumentRepository(bdd)
    document = docRepository.finddetail(id)
    if document.delete():
        flash("Document supprimé", "info")
        return redirect(url_for('urls.livres'))
    else:
        flash("Erreur de suppression", "error")

    return render_template('edition_livre.html', document=document)


@bp_admin.route('/livre/<int:id>/emprunts')
@admin_required
def empruntsLivre(id):
    bdd = get_db()
    docRepository = DocumentRepository(bdd)
    emprunts = docRepository.listEmprunts(id)

    return render_template('empruntsLivre.html', emprunts=emprunts, idDocument=id)


@bp_admin.route('/admin', methods=['GET', 'POST'])
@admin_required
def admin():
    bdd = get_db()
    utilisateurRepository = UtilisateurRepository(bdd)
    docRepository = DocumentRepository(bdd)

    #   Pour enregistrer un emprunt
    if request.method == 'POST':
        user = utilisateurRepository.get(request.form['email'])
        if user:
            livre = docRepository.finddetail(request.form['codebarre'])
            if not livre:
                flash("Livre non trouvé", 'error')
            elif livre.disponible == "disponible":
                livre.disponible = "emprunte"
                livre.update()
                if utilisateurRepository.ajoutEmprunt(user.id, livre.id, 0, 0):
                    flash("Emprunt enregistré", 'info')
                else:
                    flash("Impossible d'enregistrer l'emprunt", 'error')
            else:
                flash("Livre non disponible", 'error')
        else:
            flash("Utilisateur non trouvé", 'error')

    emprunts = utilisateurRepository.derniersEmprunts()
    utilisateurs = utilisateurRepository.dernierUtilisateurs()
    nbUtilisateurs = utilisateurRepository.nombre()
    nbDocuments = docRepository.totalDocuments()
    statsDocuments = docRepository.statsDocuments()
    nombreEmpruntTotal = docRepository.nombreEmpruntTotal()

    return render_template('admin.html', emprunts=emprunts, utilisateurs=utilisateurs, nbUtilisateurs=nbUtilisateurs,
                           nbDocuments=nbDocuments,
                           statsDocuments=statsDocuments, nombreEmpruntTotal=nombreEmpruntTotal)


@bp_admin.route('/configuration-admin', methods=['GET', 'POST'])
@admin_required
def configuration():
    if request.method == "POST":
        configuration = {"bg_color": request.form.get("bg_color"),
                         "text_color": request.form.get("text_color"),
                         "front_color": request.form.get("front_color"),
                         "nav_color": request.form.get("nav_color"),

                         "titre": request.form.get("titre"),
                         "soustitre": request.form.get("soustitre")}
        with open("configuration.json", "w") as f:
            json.dump(configuration, f)
    else:
        with open("configuration.json") as f:
            configuration = json.load(f)

    return render_template('configuration.html', configuration=configuration)
