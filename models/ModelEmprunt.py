'''
    Author: Alexis Walle
    Représente une entrée dans la table emprunt
'''
from models.Model import Model
from models.ModelUtilisateur import ModelUtilisateur
from models.ModelDocument import ModelDocument
from abc import abstractmethod


class ModelEmprunt(Model):
    def __init__(self, bdd, donnes={}):
        self.table = "Emprunt"
        super().__init__(bdd)

        #   Initiation des valeurs par défault (utile lors d'un select)
        self._Model__id = donnes.pop("id", 0)
        self.idUtilisateur = donnes.get("idUtilisateur") or 0
        self.idDocument = donnes.get("idDocument") or 0
        self.dateLimite = donnes.get("dateLimite") or 0
        self.dateRetour = donnes.get("dateRetour") or 0
        self.dateEmprunt = donnes.get("dateRetour") or 0
        self.utilisateur = ModelUtilisateur(bdd, donnes)
        self.document = ModelDocument(bdd, donnes)

    @property
    def idUtilisateur(self):
        return self.__idUtilisateur

    @idUtilisateur.setter
    def idUtilisateur(self, idUtilisateur):
        self.estModifie()
        self.__idUtilisateur = int(idUtilisateur)

    @property
    def utilisateur(self):
        return self.__utilisateur

    @utilisateur.setter
    def utilisateur(self, utilisateur):
        if type(utilisateur) != ModelUtilisateur:
            raise TypeError("l'utilisateur doit être du type ModelUtilisateur, non", type(utilisateur))
        self.estModifie()
        self.__utilisateur = utilisateur
        self.idUtilisateur = utilisateur.id

    @property
    def idDocument(self):
        return self.__idDocument

    @idDocument.setter
    def idDocument(self, idDoc):
        self.estModifie()
        self.__idDocument = int(idDoc)

    @property
    def document(self):
        return self.__document

    @document.setter
    def document(self, document):
        if type(document) != ModelDocument:
            raise TypeError("le document doit être du type ModelDocument, non", type(document))
        self.estModifie()
        self.__document = document
        self.idDocument = document.id


    @property
    def dateEmprunt(self):
        """
        :return: date à laquelle l'utilisateur a emprunté le document
        """
        return self.__dateEmprunt

    @dateEmprunt.setter
    def dateEmprunt(self, dateEmprunt):
        self.estModifie()
        self.__dateEmprunt = dateEmprunt


    @property
    def dateLimite(self):
        """
        :return: date limite d'emprunt
        """
        return self.__dateLimite

    @dateLimite.setter
    def dateLimite(self, dateLimite):
        self.estModifie()
        self.__dateLimite = dateLimite


    @property
    def dateRetour(self):
        """
        :return: la date à laquelle l'utilisateur à rendu le livre
        """
        return self.__dateRetour

    @dateRetour.setter
    def dateRetour(self, dateRetour):
        self.estModifie()
        self.__dateRetour = dateRetour


    def createTable(self):
        """
        :return: la création de la table EMPRUNT
        """
        self.bdd.execute("DROP TABLE IF EXISTS Emprunt")
        return super().createTable(
            "CREATE TABLE Emprunt(id INTEGER PRIMARY KEY ASC, idUtilisateur TEXT, idDocument TEXT, dateLimite INTEGER, dateRetour INTEGER, dateEmprunt INTEGER, FOREIGN KEY(idUtilisateur) REFERENCES Utilisateur(id), FOREIGN KEY(idDocument) REFERENCES Document(id))")

    def update(self):
        donnees = [self.idUtilisateur, self.idDocument, self.dateEmprunt, self.dateLimite, self.dateRetour, self.id]
        reponse = super().update(
            "UPDATE Emprunt SET idUtilisateur = ?, idDocument = ?, dateEmprunt = ?, dateLimite = ?, dateRetour = ? WHERE id = ?",
            donnees)
        if not reponse:
            return False
        if self.document and self.document.id > 0:
            self.document.update()
        if self.utilisateur and self.document.id > 0:
            self.utilisateur.update()
        return reponse

    def insert(self):
        donnees = [self.idUtilisateur, self.idDocument, self.dateEmprunt, self.dateLimite, self.dateRetour]
        return super().insert(
            "INSERT INTO Emprunt(idUtilisateur, idDocument, dateEmprunt, dateLimite, dateRetour) VALUES (?, ?, ?, ?, ?)", donnees)

    def delete(self):
        donnees = [self.id]
        return super().delete("DELETE FROM Emprunt WHERE id = ?", donnees)
