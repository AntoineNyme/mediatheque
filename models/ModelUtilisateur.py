'''
    Author: Alexis Walle
    Représente une entrée dans la table Utilisateur
'''
from models.Model import Model

class ModelUtilisateur(Model):
    def __init__(self, bdd, donnees={}):
        self.table = "Utilisateur"
        super().__init__(bdd)

        self._Model__id = donnees.pop("id", 0) if donnees.get("id") else donnees.pop("idUtilisateur", 0)
        self.nom = donnees.get("nom", "")
        self.nom = donnees.get("nom", "")
        self.prenom = donnees.get("prenom", "")
        self.email = donnees.get("email", "")
        self.profession = donnees.get("profession", "")
        self.peutEmprunter = donnees.get("peutEmprunter", True)
        self.estAdmin = donnees.get("estAdmin", False)

    @property
    def nom(self):
        return self.__nom

    @nom.setter
    def nom(self, nom):
        self.estModifie()
        self.__nom = nom

    @property
    def prenom(self):
        return self.__prenom

    @prenom.setter
    def prenom(self, prenom):
        self.estModifie()
        self.__prenom = prenom

    @property
    def profession(self):
        return self.__profession

    @profession.setter
    def profession(self, profession):
        self.estModifie()
        self.__profession = profession

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, email):
        self.estModifie()
        self.__email = email

    @property
    def peutEmprunter(self):
        if not self.__peutEmprunter:
            return None
        elif self.__peutEmprunter == 'oui':
            return True
        elif self.__peutEmprunter == 'non':
            return False
        else:
            return None

    @peutEmprunter.setter
    def peutEmprunter(self, possibilite):
        if possibilite:
            self.__peutEmprunter = 'oui'
        else:
            self.__peutEmprunter = 'non'
        self.estModifie()

    #   pas de getter pour le mot de passe
    @property
    def motdepasse(self):
        return None

    @motdepasse.setter
    def motdepasse(self, mdp):
        self.estModifie()
        self.__motdepasse = mdp

    @property
    def estAdmin(self):
        return self.__estAdmin

    @estAdmin.setter
    def estAdmin(self, estAdmin=False):
        self.__estAdmin = estAdmin == True

    def createTable(self):
        self.bdd.execute("DROP TABLE IF EXISTS Utilisateur")
        return super().createTable(
            "CREATE TABLE Utilisateur(id INTEGER PRIMARY KEY ASC,nom TEXT,prenom TEXT, profession TEXT, email TEXT, peutEmprunter TEXT, motdepasse TEXT, estadmin NUM)")

    def update(self):
        donnees = [self.nom, self.prenom, self.profession, self.email, self.peutEmprunter,
                   self._ModelUtilisateur__motdepasse, self.estAdmin, self.id]
        return super().update(
            "UPDATE Utilisateur SET nom = ?, prenom = ?, profession = ?, email = ?, peutEmprunter = ?, motdepasse = ?, estAdmin = ? WHERE id = ?",
            donnees)

    def delete(self):
        donnees = [self.id]
        return super().delete("DELETE FROM Utilisateur WHERE id = ?", donnees)

    def insert(self):
        donnees = [self.nom, self.prenom, self.profession, self.email, self.peutEmprunter,
                   self._ModelUtilisateur__motdepasse, self.estAdmin]
        return super().insert(
            "INSERT INTO Utilisateur(nom, prenom, profession, email, peutEmprunter, motdepasse, estAdmin) VALUES (?, ?, ?, ?, ?, ?, ?)",
            donnees)
